export default function ExpensesTable(props){

    const expenses = props.expenses

    return(<table>
        <thead><tr><th>Expenses Id</th><th>Reason</th><th>Amount</th><th>Wedding Id</th></tr></thead>
        <tbody>
            {expenses.map(e=><tr key={e.expensesId}>
            <td>{e.expensesId}</td>
            <td>{e.reason}</td>
            <td>{e.amount}</td>
            <td>{e.weddingId}</td>
            </tr>)}
        </tbody>
    </table>)
}