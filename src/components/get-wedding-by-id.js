import axios from 'axios';
import { useState,useRef } from 'react';
import WeddingTable from './wedding-table';



export default function GetWeddingById(){
    const weddingIdInput = useRef(null);
    
    const [wedding,setWedding] = useState([])
    async function getWeddingById(event){
        try{
        const id = weddingIdInput.current.value
        const response = await axios.get(`http://34.125.157.116:3000/weddings/${id}`)
        const array = [response.data]
        setWedding(array)
        }catch(error){
            alert("No wedding with that Id")
        }
    }

        return(<div>
            <h3>Get Wedding By ID</h3>
            <input placeholder="Wedding ID" ref={weddingIdInput}></input>
            <button onClick={getWeddingById}>Get Wedding By Id</button>
            <WeddingTable wedding={wedding}></WeddingTable>
        </div>)
}