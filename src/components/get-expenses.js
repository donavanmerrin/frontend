import axios from 'axios'
import {useState} from 'react'
import ExpensesTable from './expenses-table'

export default function GetExpenses(){
    const [expenses,setExpenses] = useState([])
    async function getExpenses(event){
        const response = await axios.get('http://34.125.157.116:3000/expenses')
        setExpenses(response.data)
    }

    return(<div>
        <h1>Expenses Forms</h1>
        <h3>View All Expenses</h3>
        <button onClick={getExpenses}>Get All Expenses</button>
        <ExpensesTable expenses={expenses}></ExpensesTable>
    </div>)
}