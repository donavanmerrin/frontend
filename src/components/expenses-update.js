import axios from "axios"
import { useRef } from "react"

export default function ExpensesPut(){

    const expensesIdInput = useRef()
    const expensesReasonInput = useRef()
    const expensesAmountInput = useRef()
    const weddingIdInput = useRef()

    async function putExpenses(){
        const expenses = {
            expensesId:Number(expensesIdInput.current.value),
            reason:expensesReasonInput.current.value,
            amount:expensesAmountInput.current.value,
            weddingId:Number(weddingIdInput.current.value)
        }
        try{const response = await axios.put(`http://34.125.157.116:3000/expenses/${expensesIdInput.current.value}`,expenses)
        alert("You updated an expense :)")
        console.log(response)
    }catch(error){
        alert("No expense with that ID")
    }
}

    return(<div>
        <h3>Here You Can Update An Expense</h3>
        <input placeholder="Expenses Id" ref={expensesIdInput}></input>
        <input placeholder="Expenses Reason" ref={expensesReasonInput}></input>
        <input placeholder="Expenses Amount" ref={expensesAmountInput}></input>
        <input placeholder="Wedding Id" ref={weddingIdInput}></input>
        <button onClick={putExpenses}>Update Expense</button>
    </div>)

}