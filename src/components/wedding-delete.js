import axios from "axios"
import { useRef } from "react"

export default function WeddingDeletion(){

    const weddingIdInput = useRef(null);

    async function deleteWedding(){
       try{
        const id = weddingIdInput.current.value
        const response = await axios.delete(`http://34.125.157.116:3000/weddings/${id}`)
        alert("You deleted a wedding :)")
       
       }catch(error){
           alert("No wedding with that ID")
       }
    }
    return (<div>
        <h3>Here You Can Delete A Wedding</h3>
        <input placeholder="Wedding ID" ref={weddingIdInput}></input>
        <button onClick={deleteWedding}>Delete Wedding By ID</button>
        
    </div>)
}