import axios from 'axios';
import { useState } from 'react';
import WeddingTable from './wedding-table';


export default function GetWedding(){
    
    const [wedding,setWedding] = useState([])
    async function getWedding(event){
        const response = await axios.get('http://34.125.157.116:3000/weddings')
        setWedding(response.data)
    }

        return(<div>
            <h1>Wedding and Expenses Forms</h1>
            <h1>Weddings</h1>
            <h3>View All Weddings</h3>
            <button onClick={getWedding}>Get All Weddings</button>
            <WeddingTable wedding={wedding}></WeddingTable>


        </div>)
}