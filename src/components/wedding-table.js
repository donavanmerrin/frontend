export default function WeddingTable(props){

    const wedding = props.wedding

    return(<table>
        <thead><tr><th>Wedding Id</th><th>Wedding Name</th><th>Wedding Date</th><th>Wedding Location</th><th>Wedding Budget</th></tr></thead>
        <tbody>
            {wedding.map(w =><tr key={w.weddingId}>
                <td>{w.weddingId}</td>
                <td>{w.weddingName}</td>
                <td>{w.weddingDate}</td>
                <td>{w.weddingLocation}</td>
                <td>{w.weddingBudget}</td>
            </tr>)}
        </tbody>
    </table>)
}